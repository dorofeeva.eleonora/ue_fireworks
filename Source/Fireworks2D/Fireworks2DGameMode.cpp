// Copyright Epic Games, Inc. All Rights Reserved.

#include "Fireworks2DGameMode.h"
#include "Fireworks2DCharacter.h"

AFireworks2DGameMode::AFireworks2DGameMode()
{	
	// Set default pawn class to our character
	DefaultPawnClass = AFireworks2DCharacter::StaticClass();
}
