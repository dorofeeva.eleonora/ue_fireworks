#include "Fireworks2DCharacter.h"
#include "Components/TextRenderComponent.h"
#include "Components/InputComponent.h"
#include "Components/ComboBoxString.h"
#include "Components/Slider.h"
#include "Components/CheckBox.h"
#include "GameFramework/SpringArmComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "TimerManager.h"


DEFINE_LOG_CATEGORY_STATIC(SideScrollerCharacter, Log, All);

//////////////////////////////////////////////////////////////////////////
// AFireworks2DCharacter

AFireworks2DCharacter::AFireworks2DCharacter()
{
	
	static ConstructorHelpers::FClassFinder<UUserWidget> InfoWidget_ref(TEXT("/Game/HUD/BP_InfoWidget"));
	BP_InfoWidget = InfoWidget_ref.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> MainWidget_ref(TEXT("/Game/HUD/BP_HUD"));
	BP_MainWidget = MainWidget_ref.Class;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("ParticleSystem'/Game/ParticleSystem/PS_Firework.PS_Firework'"));
	ParticleFX1 = Particle.Object;

	OnColorChanged("Gold", ESelectInfo::Direct);
	OnSizeChanged(0.5);
	isAuto = true;
}

void AFireworks2DCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	int32 sum = 0;
	int32 count = 0;
	for (auto component : Particles)
	{
		if (component)
		{
			count++;
			sum += component->GetNumActiveParticles();
		}
	}

	if (InfoWidget)
		InfoWidget->setParticlesNum(sum, count);

}

//////////////////////////////////////////////////////////////////////////
// Input

void AFireworks2DCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{	
	auto MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	MyController->bShowMouseCursor = true;
	MyController->bEnableClickEvents = true;
	MyController->bEnableMouseOverEvents = true;
	
	auto fire = PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFireworks2DCharacter::AddNewPS);

	if (BP_InfoWidget) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		InfoWidget = CreateWidget<UBase_InfoWidget>(MyController, BP_InfoWidget);

		// now you can use the widget directly since you have a referance for it.
		// Extra check to  make sure the pointer holds the widget.
		if (InfoWidget)
		{
			//let add it to the view port
			InfoWidget->AddToViewport();
		}
	}

	if (BP_MainWidget) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		MainWidget = CreateWidget<UBase_MainWidget>(MyController, BP_MainWidget);

		// now you can use the widget directly since you have a referance for it.
		// Extra check to  make sure the pointer holds the widget.
		if (MainWidget)
		{
			//let add it to the view port
			MainWidget->AddToViewport();

			//Binding
			if(MainWidget->ColorComboBox)
				MainWidget->ColorComboBox->OnSelectionChanged.AddDynamic(
					this, &AFireworks2DCharacter::OnColorChanged);

			if (MainWidget->SizeSlider)
				MainWidget->SizeSlider->OnValueChanged.AddDynamic(
					this, &AFireworks2DCharacter::OnSizeChanged);

			if (MainWidget->AutoPlayCheckBox)
				MainWidget->AutoPlayCheckBox->OnCheckStateChanged.AddDynamic(
					this, &AFireworks2DCharacter::OnAutoChecked);
		}
	}

	//Start timer for auto play
	GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &AFireworks2DCharacter::ShootAtRandomPoint, 1.0f, true, 1.0f);
}

void AFireworks2DCharacter::AddNewPS()
{
	auto MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	FVector WorldLocation, WorldDirection;
	MyController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
	
	ShootAtLocation(WorldLocation);
}

void AFireworks2DCharacter::OnColorChanged(FString sItem, ESelectInfo::Type seltype)
{
	if (sItem == FString("Red"))
	{
		CurrentColor = FLinearColor(FVector(255,0,0));
	}
	else if (sItem == FString("Green"))
	{
		CurrentColor = FLinearColor(FVector(0, 255, 0));
	}
	else if (sItem == FString("Blue"))
	{
		CurrentColor = FLinearColor(FVector(0, 0, 255));
	}
	else if (sItem == FString("Gold"))
	{
		CurrentColor = FLinearColor(FVector(1, 1, 1));
	}


}

void AFireworks2DCharacter::OnSizeChanged(float InValue)
{
	CurrentRadius = 6000 * InValue;
}

void AFireworks2DCharacter::OnAutoChecked(bool Checked)
{
	isAuto = Checked;
}

void AFireworks2DCharacter::ShootAtRandomPoint()
{
	if (isAuto)
	{
		auto MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		int32 x = 0, y = 0;
		MyController->GetViewportSize(x, y);

		FVector WorldLocation, WorldDirection;
		float newX = FMath::RandRange(10, x - 10);
		float newY = FMath::RandRange(10, y - 10);

		MyController->DeprojectScreenPositionToWorld(newX, newY, WorldLocation, WorldDirection);
		ShootAtLocation(WorldLocation);
	}
}

void AFireworks2DCharacter::ShootAtLocation(FVector location)
{
	FTransform transform(location);
	if (ParticleFX1)
	{
		auto component = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),
			ParticleFX1, transform);
		if (component)
		{
			Particles.push_back(component);

			auto material = component->CreateDynamicMaterialInstance(2);
			if (material)
				material->SetVectorParameterValue("Color", CurrentColor);

			component->SetMaterial(2, material);

			component->SetFloatParameter("BoomRadius", CurrentRadius);
		}
	}
}