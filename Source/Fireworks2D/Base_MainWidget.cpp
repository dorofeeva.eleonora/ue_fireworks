// Fill out your copyright notice in the Description page of Project Settings.
#include "Base_MainWidget.h"

bool UBase_MainWidget::Initialize()
{
    if (!Super::Initialize())
    {
        return false;
    }

    UWidgetBlueprintGeneratedClass* BGClass = Cast<UWidgetBlueprintGeneratedClass>(GetClass());
    if (BGClass != nullptr)
    {
        BGClass->InitializeWidget(this);
    }
    return true;
}