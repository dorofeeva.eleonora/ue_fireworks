// Fill out your copyright notice in the Description page of Project Settings.


#include "Base_InfoWidget.h"
#include "Components/TextBlock.h"

bool UBase_InfoWidget::Initialize()
{
    if (!Super::Initialize())
    {
        return false;
    }

    // Only do this if this widget is of a blueprint class  
    UWidgetBlueprintGeneratedClass* BGClass = Cast<UWidgetBlueprintGeneratedClass>(GetClass());
    if (BGClass != nullptr)
    {
        BGClass->InitializeWidget(this);
    }
	return true;
}

void UBase_InfoWidget::setParticlesNum(const int32& sum, const int32& count)
{
    if (ParticlesTextBlock)
    {
        ParticlesTextBlock->SetText(FText::FromString(FString::FromInt(sum)));
    }

    if (EmitersTextBlock)
    {
        EmitersTextBlock->SetText(FText::FromString(FString::FromInt(count)));
    }
}
