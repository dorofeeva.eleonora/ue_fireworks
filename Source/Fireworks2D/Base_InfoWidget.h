// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Base_InfoWidget.generated.h"

/**
 * 
 */
UCLASS()
class FIREWORKS2D_API UBase_InfoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* ParticlesTextBlock;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* EmitersTextBlock;

	void setParticlesNum(const int32& sum, const int32& count);
protected:
	bool Initialize() override;
};
