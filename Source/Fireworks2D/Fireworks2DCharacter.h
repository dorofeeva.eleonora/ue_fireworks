// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "Base_InfoWidget.h"
#include "Base_MainWidget.h"
#include "Fireworks2DCharacter.generated.h"

class UTextRenderComponent;
class UParticleSystem;
class UParticleSystemComponent;

/**
 * This class is the default character for Fireworks2D, and it is responsible for all
 * physical interaction between the player and the world.
 *
 * The capsule component (inherited from ACharacter) handles collision with the world
 * The CharacterMovementComponent (inherited from ACharacter) handles movement of the collision capsule
 * The Sprite component (inherited from APaperCharacter) handles the visuals
 */
UCLASS(config=Game)
class AFireworks2DCharacter : public APaperCharacter
{
	GENERATED_BODY()

	UPROPERTY()
	UParticleSystem* ParticleFX1;

	// Reference UMG Asset in the Editor
	UPROPERTY()
	TSubclassOf<class UBase_InfoWidget> BP_InfoWidget;

	// Variable to hold the widget After Creating it.
	UPROPERTY()
	UBase_InfoWidget* InfoWidget;

	// Reference UMG Asset in the Editor
	UPROPERTY()
	TSubclassOf<class UBase_MainWidget> BP_MainWidget;

public:
	// Variable to hold the widget After Creating it.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	UBase_MainWidget* MainWidget;

	virtual void Tick(float DeltaSeconds) override;
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	void AddNewPS();

	UFUNCTION()
	void OnColorChanged(FString sItem, ESelectInfo::Type seltype);

	UFUNCTION()
	void OnSizeChanged(float InValue);

	UFUNCTION()
	void OnAutoChecked(bool Checked);

	UFUNCTION()
	void ShootAtRandomPoint();

	void ShootAtLocation(FVector location);
protected:
	std::vector<UParticleSystemComponent*> Particles;
	FLinearColor CurrentColor;
	float CurrentRadius;
	bool isAuto;

	FTimerHandle MemberTimerHandle;

public:
	AFireworks2DCharacter();
};
