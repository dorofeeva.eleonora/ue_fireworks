#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Base_MainWidget.generated.h"

class UComboBoxString;
/**
 * 
 */
UCLASS()
class FIREWORKS2D_API UBase_MainWidget : public UUserWidget
{
	GENERATED_BODY()

	bool Initialize() override;
public:

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UComboBoxString* ColorComboBox;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class USlider* SizeSlider;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UCheckBox* AutoPlayCheckBox;
};
