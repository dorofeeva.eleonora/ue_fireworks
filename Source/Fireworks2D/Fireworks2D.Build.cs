// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Fireworks2D : ModuleRules
{
	public Fireworks2D(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Paper2D", "Slate", "SlateCore", "UMG" });
	}
}
