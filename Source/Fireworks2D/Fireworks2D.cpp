// Copyright Epic Games, Inc. All Rights Reserved.

#include "Fireworks2D.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Fireworks2D, "Fireworks2D" );
